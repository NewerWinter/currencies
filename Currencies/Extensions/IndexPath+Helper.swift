//
//  IndexPath+Helper.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 13/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import Foundation

public extension IndexPath {
	
	/// Create IndexPath for row 0 and section 0
	public static var first: IndexPath {
		return IndexPath(row: 0, section: 0)
	}
}
