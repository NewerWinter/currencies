//
//  UITableViewCell+Helper.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 11/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import UIKit

extension UITableViewCell {

	/// Create reuseID for UITableViewCell from class name
	///
	/// - Returns: reuseID
	public class func identifier() -> String {
		return String(NSStringFromClass(self).split(separator: ".").last!)
	}
}

extension UITableViewHeaderFooterView {

	/// Create reuseID for UITableViewHeaderFooterView from class name
	///
	/// - Returns: reuseID
	public class func identifier() -> String {
		return String(NSStringFromClass(self).split(separator: ".").last!)
	}
}
