//
//  Stroyboard+Helper.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 14/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import UIKit

public extension UIViewController {

	/// Create ViewController from Main Storyboard
	///
	/// - Returns: view controller with self class
	public static func instantiateFromStoryboard() -> Self {
		return _instantiateFromStoryboardHelper(self,
												storyboardName: _defaultStoryboardName)
	}

	/// Create ViewController from Storyboard with specific name
	///
	/// - Parameter storyboardName: Name of storyboard where view controller is
	/// - Returns: view controller with self class
	public static func instantiateFromStoryboard(_ storyboardName: String) -> Self {
		return _instantiateFromStoryboardHelper(self, storyboardName: storyboardName)
	}

	fileprivate static func _instantiateFromStoryboardHelper<T>(
		_ type: T.Type,
		storyboardName: String) -> T! {
		var storyboardId = ""
		let components = "\(Swift.type(of: type))".components(separatedBy: ".")

		if components.count > 1 {
			storyboardId = components[0]
		}
		let storyboad = UIStoryboard(name: storyboardName, bundle: nil)
		let controller = storyboad.instantiateViewController(withIdentifier: storyboardId) as? T

		return controller
	}

	fileprivate static var _defaultStoryboardName: String {
		return "Main"
	}
}
