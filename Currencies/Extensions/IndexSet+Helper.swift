//
//  IndexSet+Helper.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 15/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import Foundation

public extension IndexSet {

	/// Create Index Set [0]
	public static var first: IndexSet {
		return [0]
	}
}
