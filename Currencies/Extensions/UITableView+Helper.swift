//
//  UITableView+Helper.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 11/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import UIKit

extension UITableView {

	/// Method for type safe register cell in table with class
	///
	/// - Parameter cellClass: cell class, that should be register
	public func register<T: UITableViewCell> (_ cellClass: T.Type) {
		self.register(cellClass, forCellReuseIdentifier: cellClass.identifier())
	}

	/// Method for type safe register cell in table with nib and class
	///
	/// - Parameters:
	///   - nib: nib that store cell
	///   - cellClass: cell class that should register
	public func register<T: UITableViewCell>(_ nib: UINib, forClass cellClass: T.Type) {
		self.register(nib, forCellReuseIdentifier: cellClass.identifier())
	}

	/// Method for type safe cell dequeue
	///
	/// - Parameter cellClass: class to dequeue
	/// - Returns: cell with T class
	public func dequeueReusableCell<T: UITableViewCell> (withClass cellClass: T.Type) -> T? {
		return self.dequeueReusableCell(withIdentifier: cellClass.identifier()) as? T
	}
	
	/// Method for type safe cell dequeue for index path
	///
	/// - Parameters:
	///   - cellClass: class to dequeue
	///   - indexPath: path to dequeue
	/// - Returns: cell with T class
	public func dequeueReusableCell<T: UITableViewCell>(
		withClass cellClass:
		T.Type,
		forIndexPath indexPath: IndexPath) -> T {
		guard let cell = self.dequeueReusableCell(
			withIdentifier: cellClass.identifier(), for: indexPath) as? T else {
				fatalError("Error: cannot dequeue cell with identifier: \(cellClass.identifier()) " +
					"for index path: \(indexPath)")
		}
		return cell
	}

	/// Method for type safe header register in table with class
	///
	/// - Parameter headerFooterClass: header class to register
	public func register<T: UITableViewHeaderFooterView>(_ headerFooterClass: T.Type) {
		self.register(headerFooterClass,
					  forHeaderFooterViewReuseIdentifier: headerFooterClass.identifier())
	}

	/// Method for type safe header register in table with nib and class
	///
	/// - Parameters:
	///   - nib: nib that store header
	///   - headerFooterClass: header class to register
	public func register<T: UITableViewHeaderFooterView>(
		_ nib: UINib,
		forHeaderFooterClass headerFooterClass: T.Type) {
		self.register(nib, forHeaderFooterViewReuseIdentifier: headerFooterClass.identifier())
	}

	/// Method for type safe header dequeue
	///
	/// - Parameter headerFooterClass: header class to dequeue
	/// - Returns: header with T class
	public func dequeueResuableHeaderFooterView<T: UITableViewHeaderFooterView>(
		withClass headerFooterClass: T.Type) -> T? {
		return dequeueReusableHeaderFooterView(withIdentifier: headerFooterClass.identifier()) as? T
	}
}
