// Generated using Sourcery 0.15.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

// swiftlint:disable file_length
fileprivate func compareOptionals<T>(lhs: T?, rhs: T?, compare: (_ lhs: T, _ rhs: T) -> Bool) -> Bool {
    switch (lhs, rhs) {
    case let (lValue?, rValue?):
        return compare(lValue, rValue)
    case (nil, nil):
        return true
    default:
        return false
    }
}

fileprivate func compareArrays<T>(lhs: [T], rhs: [T], compare: (_ lhs: T, _ rhs: T) -> Bool) -> Bool {
    guard lhs.count == rhs.count else { return false }
    for (idx, lhsItem) in lhs.enumerated() {
        guard compare(lhsItem, rhs[idx]) else { return false }
    }

    return true
}


// MARK: - AutoEquatable for classes, protocols, structs
// MARK: - CurrencyCellViewModel AutoEquatable
extension CurrencyCellViewModel: Equatable {}
func == (lhs: CurrencyCellViewModel, rhs: CurrencyCellViewModel) -> Bool {
    guard lhs.value == rhs.value else { return false }
    guard lhs.isEditing == rhs.isEditing else { return false }
    guard lhs.rateModel == rhs.rateModel else { return false }
    return true
}
// MARK: - CurrencyModel AutoEquatable
extension CurrencyModel: Equatable {}
func == (lhs: CurrencyModel, rhs: CurrencyModel) -> Bool {
    guard lhs.base == rhs.base else { return false }
    guard lhs.date == rhs.date else { return false }
    guard lhs.rateList == rhs.rateList else { return false }
    return true
}
// MARK: - CurrencyRateModel AutoEquatable
extension CurrencyRateModel: Equatable {}
func == (lhs: CurrencyRateModel, rhs: CurrencyRateModel) -> Bool {
    guard lhs.currency == rhs.currency else { return false }
    guard lhs.rate == rhs.rate else { return false }
    return true
}

// MARK: - AutoEquatable for Enums
