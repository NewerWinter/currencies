// Generated using Sourcery 0.15.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

// swiftlint:disable line_length
// swiftlint:disable variable_name

import Foundation
#if os(iOS) || os(tvOS) || os(watchOS)
import UIKit
#elseif os(OSX)
import AppKit
#endif













class CurrencyCellViewModelFactoryInputMock: CurrencyCellViewModelFactoryInput {

    //MARK: - createCellViewModelList

    var createCellViewModelListFromDefaultValueCallsCount = 0
    var createCellViewModelListFromDefaultValueCalled: Bool {
        return createCellViewModelListFromDefaultValueCallsCount > 0
    }
    var createCellViewModelListFromDefaultValueReceivedArguments: (currencyModel: CurrencyModel, defaultValue: Double)?
    var createCellViewModelListFromDefaultValueReturnValue: [CurrencyCellViewModel]!
    var createCellViewModelListFromDefaultValueClosure: ((CurrencyModel, Double) -> [CurrencyCellViewModel])?

    func createCellViewModelList(	from currencyModel: CurrencyModel,	defaultValue: Double) -> [CurrencyCellViewModel] {
        createCellViewModelListFromDefaultValueCallsCount += 1
        createCellViewModelListFromDefaultValueReceivedArguments = (currencyModel: currencyModel, defaultValue: defaultValue)
        return createCellViewModelListFromDefaultValueClosure.map({ $0(currencyModel, defaultValue) }) ?? createCellViewModelListFromDefaultValueReturnValue
    }

    //MARK: - createCellViewModel

    var createCellViewModelFromDefaultValueCallsCount = 0
    var createCellViewModelFromDefaultValueCalled: Bool {
        return createCellViewModelFromDefaultValueCallsCount > 0
    }
    var createCellViewModelFromDefaultValueReceivedArguments: (rateModel: CurrencyRateModel, defaultValue: Double)?
    var createCellViewModelFromDefaultValueReturnValue: CurrencyCellViewModel!
    var createCellViewModelFromDefaultValueClosure: ((CurrencyRateModel, Double) -> CurrencyCellViewModel)?

    func createCellViewModel(	from rateModel: CurrencyRateModel,	defaultValue: Double) -> CurrencyCellViewModel {
        createCellViewModelFromDefaultValueCallsCount += 1
        createCellViewModelFromDefaultValueReceivedArguments = (rateModel: rateModel, defaultValue: defaultValue)
        return createCellViewModelFromDefaultValueClosure.map({ $0(rateModel, defaultValue) }) ?? createCellViewModelFromDefaultValueReturnValue
    }

    //MARK: - createCellViewModelList

    var createCellViewModelListFromCallsCount = 0
    var createCellViewModelListFromCalled: Bool {
        return createCellViewModelListFromCallsCount > 0
    }
    var createCellViewModelListFromReceivedCurrencyModel: CurrencyModel?
    var createCellViewModelListFromReturnValue: [CurrencyCellViewModel]!
    var createCellViewModelListFromClosure: ((CurrencyModel) -> [CurrencyCellViewModel])?

    func createCellViewModelList(from currencyModel: CurrencyModel) -> [CurrencyCellViewModel] {
        createCellViewModelListFromCallsCount += 1
        createCellViewModelListFromReceivedCurrencyModel = currencyModel
        return createCellViewModelListFromClosure.map({ $0(currencyModel) }) ?? createCellViewModelListFromReturnValue
    }

    //MARK: - createCellViewModel

    var createCellViewModelFromCallsCount = 0
    var createCellViewModelFromCalled: Bool {
        return createCellViewModelFromCallsCount > 0
    }
    var createCellViewModelFromReceivedRateModel: CurrencyRateModel?
    var createCellViewModelFromReturnValue: CurrencyCellViewModel!
    var createCellViewModelFromClosure: ((CurrencyRateModel) -> CurrencyCellViewModel)?

    func createCellViewModel(from rateModel: CurrencyRateModel) -> CurrencyCellViewModel {
        createCellViewModelFromCallsCount += 1
        createCellViewModelFromReceivedRateModel = rateModel
        return createCellViewModelFromClosure.map({ $0(rateModel) }) ?? createCellViewModelFromReturnValue
    }

}
class CurrencyServiceInputMock: CurrencyServiceInput {
    var isLoading: Bool {
        get { return underlyingIsLoading }
        set(value) { underlyingIsLoading = value }
    }
    var underlyingIsLoading: Bool!

    //MARK: - get

    var getForSuccessCallbackFailureCallbackCallsCount = 0
    var getForSuccessCallbackFailureCallbackCalled: Bool {
        return getForSuccessCallbackFailureCallbackCallsCount > 0
    }
    var getForSuccessCallbackFailureCallbackReceivedArguments: (currency: Currency, successCallback: SuccessCallback, failureCallback: FailureCallback)?
    var getForSuccessCallbackFailureCallbackClosure: ((Currency, @escaping SuccessCallback, @escaping FailureCallback) -> Void)?

    func get(		for currency: Currency,		successCallback: @escaping SuccessCallback,		failureCallback: @escaping FailureCallback) {
        getForSuccessCallbackFailureCallbackCallsCount += 1
        getForSuccessCallbackFailureCallbackReceivedArguments = (currency: currency, successCallback: successCallback, failureCallback: failureCallback)
        getForSuccessCallbackFailureCallbackClosure?(currency, successCallback, failureCallback)
    }

}
class CurrencyTableViewManagerDataSourceMock: CurrencyTableViewManagerDataSource {

    //MARK: - numberOfRows

    var numberOfRowsForSectionCallsCount = 0
    var numberOfRowsForSectionCalled: Bool {
        return numberOfRowsForSectionCallsCount > 0
    }
    var numberOfRowsForSectionReceivedSection: Int?
    var numberOfRowsForSectionReturnValue: Int!
    var numberOfRowsForSectionClosure: ((Int) -> Int)?

    func numberOfRows(forSection section: Int) -> Int {
        numberOfRowsForSectionCallsCount += 1
        numberOfRowsForSectionReceivedSection = section
        return numberOfRowsForSectionClosure.map({ $0(section) }) ?? numberOfRowsForSectionReturnValue
    }

    //MARK: - currencyViewModel

    var currencyViewModelForRowAtCallsCount = 0
    var currencyViewModelForRowAtCalled: Bool {
        return currencyViewModelForRowAtCallsCount > 0
    }
    var currencyViewModelForRowAtReceivedIndexPath: IndexPath?
    var currencyViewModelForRowAtReturnValue: CurrencyCellViewModel!
    var currencyViewModelForRowAtClosure: ((IndexPath) -> CurrencyCellViewModel)?

    func currencyViewModel(forRowAt indexPath: IndexPath) -> CurrencyCellViewModel {
        currencyViewModelForRowAtCallsCount += 1
        currencyViewModelForRowAtReceivedIndexPath = indexPath
        return currencyViewModelForRowAtClosure.map({ $0(indexPath) }) ?? currencyViewModelForRowAtReturnValue
    }

}
class CurrencyTableViewManagerDelegateMock: CurrencyTableViewManagerDelegate {

    //MARK: - currencyCellSelected

    var currencyCellSelectedAtCallsCount = 0
    var currencyCellSelectedAtCalled: Bool {
        return currencyCellSelectedAtCallsCount > 0
    }
    var currencyCellSelectedAtReceivedIndexPath: IndexPath?
    var currencyCellSelectedAtClosure: ((IndexPath) -> Void)?

    func currencyCellSelected(at indexPath: IndexPath) {
        currencyCellSelectedAtCallsCount += 1
        currencyCellSelectedAtReceivedIndexPath = indexPath
        currencyCellSelectedAtClosure?(indexPath)
    }

    //MARK: - currencyViewModelEditing

    var currencyViewModelEditingNewAmountValueCallsCount = 0
    var currencyViewModelEditingNewAmountValueCalled: Bool {
        return currencyViewModelEditingNewAmountValueCallsCount > 0
    }
    var currencyViewModelEditingNewAmountValueReceivedArguments: (editingViewModel: CurrencyCellViewModel, newAmountValue: String)?
    var currencyViewModelEditingNewAmountValueClosure: ((CurrencyCellViewModel, String) -> Void)?

    func currencyViewModelEditing(		_ editingViewModel: CurrencyCellViewModel,		newAmountValue: String) {
        currencyViewModelEditingNewAmountValueCallsCount += 1
        currencyViewModelEditingNewAmountValueReceivedArguments = (editingViewModel: editingViewModel, newAmountValue: newAmountValue)
        currencyViewModelEditingNewAmountValueClosure?(editingViewModel, newAmountValue)
    }

}
