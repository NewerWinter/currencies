//
//  AutoEquatable.swift
//  CurrenciesTests
//
//  Created by Aleksandr Lavrinenko on 14/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import Foundation

protocol AutoEquatable {}
