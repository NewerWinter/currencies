//
//  AppDelegate.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 11/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	var window: UIWindow?
	
	func application(
		_ application: UIApplication,
		didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

		let viewController = CurrencyFlowAssembly.createCurrencyFlowController()
		let navigationController = UINavigationController(rootViewController: viewController)
		window?.rootViewController = navigationController

		return true
	}
}
