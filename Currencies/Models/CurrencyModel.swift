//
//  CurrencyModel.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 11/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import Foundation
import ObjectMapper

struct CurrencyModel: AutoEquatable {

	let base: Currency
	let date: String
	let rateList: [CurrencyRateModel]

	private static var _baseRate: Double {
		return 1.0
	}

	// MARK: - Keys for JSON parsing
	private static var _baseKey: String {
		return "base"
	}

	private static  var _dateKey: String {
		return "date"
	}

	private static  var _ratesKey: String {
		return "rates"
	}
}

// MARK: - ImmutableMappable
extension CurrencyModel: ImmutableMappable {
	init(map: Map) throws {
		guard let baseString = map.JSON[CurrencyModel._baseKey] as? String,
			let baseEnum = Currency(rawValue: baseString),
			let date = map.JSON[CurrencyModel._dateKey] as? String,
			let rateListDict = map.JSON[CurrencyModel._ratesKey] as? [String: Double] else {
				throw Network.Error.parseError
		}
		var rateList = [CurrencyRateModel(currency: baseEnum, rate: CurrencyModel._baseRate)]
		rateList = rateListDict.reduce(rateList) { (result, dictionary) -> [CurrencyRateModel] in
			guard let currency = Currency(rawValue: dictionary.key),
			dictionary.value > 0 else {
				return result
			}
			let rateModel = CurrencyRateModel(currency: currency, rate: dictionary.value)
			return result + [rateModel]
		}
		self.base = baseEnum
		self.date = date
		self.rateList = rateList
	}

	mutating func mapping(map: Map) {

	}
}
