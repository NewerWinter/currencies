//
//  CurrencyRateModel.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 12/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import Foundation

struct CurrencyRateModel: AutoEquatable {
	
	let currency: Currency
	let rate: Double
}
