//
//  Currencies.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 13/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import Foundation
import UIKit.UIImage

/// Currency variables
///
/// - EUR: Euro
/// - USD: US Dollar
/// - AUD: Australlian Dollar
/// - CZK: Czech Koruna
/// - CAD: Canadian Dollar
/// - JPY: Japanese Yen
/// - GBP: British Pound
enum Currency: String {
	case EUR
	case USD
	case AUD
	case CZK
	case CAD
	case JPY
	case GBP

	var description: String {
		switch self {
		case .EUR:
			return "Euro"
		case .USD:
			return "US Dollar"
		case .AUD:
			return "Australlian Dollar"
		case .CZK:
			return "Czech Koruna"
		case .CAD:
			return "Canadian Dollar"
		case .JPY:
			return "Japanese Yen"
		case .GBP:
			return "British Pound"
		}
	}

	var icon: UIImage? {
		return UIImage(named: "moneyIcon")
	}
}
