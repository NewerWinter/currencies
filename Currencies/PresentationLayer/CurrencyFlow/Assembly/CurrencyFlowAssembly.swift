//
//  CurrencyFlowAssembly.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 14/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import UIKit

final class CurrencyFlowAssembly {

	static func createCurrencyFlowController() -> UIViewController {
		let networkManager = Network.Manager<CurrencyModel>()
		let currencyServcie: CurrencyServiceInput = CurrencyService(networkManager: networkManager)
		let cellFactory: CurrencyCellViewModelFactoryInput = CurrencyCellViewModelFactory()

		let currencyListViewModel = CurrencyListViewModel(currencyServcie: currencyServcie, cellFactory: cellFactory)

		let currencyTableViewManager = CurrencyTableViewManager(delegate: currencyListViewModel)
		currencyListViewModel.setTableViewManager(currencyTableViewManager)

		let viewController = CurrencyViewController.instantiateFromStoryboard()
		viewController.currencyListViewModel = currencyListViewModel
		
		return viewController
	}
}
