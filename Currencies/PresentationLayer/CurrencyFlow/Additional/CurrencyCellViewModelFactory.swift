//
//  CurrencyCellViewModelFactory.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 11/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import Foundation

protocol CurrencyCellViewModelFactoryInput: AutoMockable {

	/// Create array of cell view model from Currency model
	///
	/// - Parameters:
	///   - currencyModel: model with list of rate models and base currency
	///   - defaultValue: start ammount, default 100
	/// - Returns: Array of cell view models
	func createCellViewModelList(
	from currencyModel: CurrencyModel,
	defaultValue: Double) -> [CurrencyCellViewModel]

	/// Create single view model from rate model
	///
	/// - Parameter rateModel: model with currency and ratio info
	/// - Returns: Cell view model
	func createCellViewModel(
	from rateModel: CurrencyRateModel,
	defaultValue: Double) -> CurrencyCellViewModel

	/// Create array of cell view model from Currency model with one hundren money amount * ratio
	///
	/// - currencyModel: model with list of rate models and base currency
	/// - Returns: Array of cell view models
	func createCellViewModelList(from currencyModel: CurrencyModel) -> [CurrencyCellViewModel]

	/// Create single view model from rate model with one hundred money amout * ratio
	///
	/// - Parameter rateModel: view model with currency and ratio info
	func createCellViewModel(from rateModel: CurrencyRateModel) -> CurrencyCellViewModel
}

extension CurrencyCellViewModelFactory {
	func createCellViewModelList(from currencyModel: CurrencyModel) -> [CurrencyCellViewModel] {
		return createCellViewModelList(from: currencyModel, defaultValue: defaultAmountValue())
	}

	func createCellViewModel(from rateModel: CurrencyRateModel) -> CurrencyCellViewModel {
		return createCellViewModel(from: rateModel, defaultValue: defaultAmountValue())
	}

	func defaultAmountValue() -> Double {
		return 100
	}
}

/// Produce CellViewModel
final class CurrencyCellViewModelFactory: CurrencyCellViewModelFactoryInput {
	
	func createCellViewModelList(
		from currencyModel: CurrencyModel,
		defaultValue: Double) -> [CurrencyCellViewModel] {
		let viewModelList = currencyModel.rateList.map { (rateModel) -> CurrencyCellViewModel in
			return CurrencyCellViewModel(rateModel: rateModel, defaultValue: defaultValue * rateModel.rate)
		}
		return viewModelList
	}
	
	func createCellViewModel(
		from rateModel: CurrencyRateModel,
		defaultValue: Double) -> CurrencyCellViewModel {
		return CurrencyCellViewModel(rateModel: rateModel, defaultValue: defaultValue * rateModel.rate)
	}
}
