//
//  CurrencyTableViewManager.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 13/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import UIKit

typealias CurrencyTableViewDelegate = UITableViewDelegate & UITableViewDataSource & UIScrollViewDelegate

/// Protocol represents data for tableView
protocol CurrencyTableViewManagerDataSource: class, AutoMockable {
	
	/// Get number of rows for the controller table
	///
	/// - Parameter section: number of section
	/// - Returns: number of tows
	func numberOfRows(forSection section: Int) -> Int

	/// Get currency view model for index path
	///
	/// - Parameter indexPath: path for cell
	/// - Returns: cell view model
	func currencyViewModel(forRowAt indexPath: IndexPath) -> CurrencyCellViewModel
}

/// Protocol delegates event handling from tableView
protocol CurrencyTableViewManagerDelegate: class, AutoMockable {

	/// Action that triggers after cellecting text field in cell
	///
	/// - Parameter indexPath: cell index path
	func currencyCellSelected(at indexPath: IndexPath)

	/// Call than user edit currency text
	///
	/// - Parameters:
	///   - editingViewModel: view model that should change
	///   - newAmountValue: amount value
	func currencyViewModelEditing(
		_ editingViewModel: CurrencyCellViewModel,
		newAmountValue: String)
}

class CurrencyTableViewManager: NSObject, CurrencyTableViewDelegate {

	unowned var delegate: CurrencyTableViewManagerDataSource & CurrencyTableViewManagerDelegate

	required init(delegate: CurrencyTableViewManagerDataSource & CurrencyTableViewManagerDelegate) {
		self.delegate = delegate
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return delegate.numberOfRows(forSection: section)
	}

	func tableView(
		_ tableView: UITableView,
		cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cellViewModel = delegate.currencyViewModel(forRowAt: indexPath)
		let cell = tableView.dequeueReusableCell(withClass: CurrencyCell.self, forIndexPath: indexPath)
		cellViewModel.configure(cell)

		cell.textDidBeginEditing = { [weak self, weak cell] in
			guard let cell = cell,
				let indexPath = tableView.indexPath(for: cell),
				let `self` = self else {
					return
			}
			self.delegate.currencyCellSelected(at: indexPath)
		}

		cell.textChangedCallback = { [weak self, weak cell] text in
			guard let cell = cell,
				let cellViewModel = cell.cellViewModel,
				let `self` = self else {
					return
			}
			self.delegate.currencyViewModelEditing(cellViewModel, newAmountValue: text.replacingOccurrences(of: ",", with: ""))
		}
		return cell
	}

	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		scrollView.endEditing(true)
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
}
