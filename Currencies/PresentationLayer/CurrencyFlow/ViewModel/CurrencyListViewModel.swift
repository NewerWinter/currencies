//
//  CurrencyListViewModel.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 11/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import Foundation
import UIKit.UITableView

/// ViewModel for the list of currencies
final class CurrencyListViewModel {
	
	// MARK: - Public properties
	
	var updateTableViewCallback: (() -> Void)?
	var moveRowCallback: ((_ fromIndexPath: IndexPath, _ toIndexPath: IndexPath) -> Void)?
	var showMessageViewCallback: ((_ show: Bool, _ text: String?) -> Void)?
	
	// MARK: - Private properties
	
	private var _cellViewModelList: [CurrencyCellViewModel] = [] {
		didSet {
			self.updateTableViewCallback?()
		}
	}
	
	private let _timeInterval: Double = 1
	private let _baseCurrency: Currency = .EUR
	
	private var _numberOfRows: Int {
		return _cellViewModelList.count
	}
	
	private var _timer = Timer()
	
	private let _currencyServcie: CurrencyServiceInput
	private let _cellFactory: CurrencyCellViewModelFactoryInput
	
	private var _tableViewManager: CurrencyTableViewManager?
	
	// MARK: - Public methods
	
	init(
		currencyServcie: CurrencyServiceInput,
		cellFactory: CurrencyCellViewModelFactoryInput) {
		_currencyServcie = currencyServcie
		_cellFactory = cellFactory
	}

	deinit {
		_timer.invalidate()
	}

	func setTableViewManager(_ tableViewManager: CurrencyTableViewManager) {
		_tableViewManager = tableViewManager
	}
	
	func setTableDelegate(for tableView: UITableView) {
		tableView.delegate = _tableViewManager
		tableView.dataSource = _tableViewManager
	}

	func sync() {
		_timer = Timer.scheduledTimer(timeInterval: _timeInterval,
									  target: self,
									  selector: CurrencyListViewModel._loadDataSelector,
									  userInfo: nil,
									  repeats: true)
	}
	
	// MARK: - Private methods
	
	@objc private func _loadData() {
		let successCallback = { [weak self] (currencyModel: CurrencyModel) in
			guard let `self` = self else {
				return
			}
			self.showMessageViewCallback?(false, nil)

			if (self._cellViewModelList.isEmpty) {
				let cellViewModelList = self._cellFactory.createCellViewModelList(from: currencyModel)
				self._cellViewModelList = cellViewModelList
				return
			}
			self._updateViewModelList(from: currencyModel)
		}
		
		let failureCallback = { [weak self] (errorText: String) in
			guard let `self` = self else {
				return
			}
			self.showMessageViewCallback?(true, errorText)
		}
		
		guard !_currencyServcie.isLoading else {
			failureCallback(Network.Error.loadingError.localizedDescription)
			return
		}
		
		_currencyServcie.get(for: _baseCurrency,
							 successCallback: successCallback,
							 failureCallback: failureCallback)
	}

	private func _updateViewModelList(from currencyModel: CurrencyModel) {
		currencyModel.rateList.forEach({ (rateModel) in
			if let viewModel = self._cellViewModelList.first(where: {$0.currency == rateModel.currency}) {
				viewModel.updateModelIfNeeded(from: rateModel)
			}
		})
	}
}

extension CurrencyListViewModel: CurrencyTableViewManagerDataSource {
	
	func currencyViewModel(forRowAt indexPath: IndexPath) -> CurrencyCellViewModel {
		guard indexPath.row < _numberOfRows else {
			fatalError("Row index larger than number of rows")
		}
		return _cellViewModelList[indexPath.row]
		
	}
	
	func numberOfRows(forSection section: Int) -> Int {
		return _numberOfRows
	}
}

// MARK: - CurrencyTableViewActionDelegate
extension CurrencyListViewModel: CurrencyTableViewManagerDelegate {
	
	func currencyCellSelected(at indexPath: IndexPath) {
		moveRowCallback?(indexPath, IndexPath.first)
	}
	
	func currencyViewModelEditing(
		_ editingViewModel: CurrencyCellViewModel,
		newAmountValue: String) {
		let newDoubleValue = Double(newAmountValue) ?? 0
		editingViewModel.value = newDoubleValue
		let baseDoubleValue = newDoubleValue / editingViewModel.rateModel.rate
		for cellViewModel in _cellViewModelList where !cellViewModel.isEditing {
			cellViewModel.value =  cellViewModel.convert(from: baseDoubleValue)
		}
	}
}

// MARK: - Selector extension
fileprivate extension CurrencyListViewModel {
	fileprivate static let _loadDataSelector = #selector(CurrencyListViewModel._loadData)
}
