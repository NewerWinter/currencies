//
//  CurrencyCellViewModel..swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 11/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import Foundation
import UIKit.UIImage

/// ViewModel for currency table cells
class CurrencyCellViewModel: AutoEquatable {
	
	var title: String {
		return currency.rawValue
	}
	
	var description: String {
		return currency.description
	}

	var icon: UIImage? {
		return currency.icon
	}
	
	var valueString: String {
		let number = NSNumber(value: value)
		return _decimalFormatter.string(from: number) ?? "0"
	}
	
	var value: Double {
		didSet {
			valueChanges?(valueString)
		}
	}
	
	var currency: Currency {
		return rateModel.currency
	}

	// sourcery:begin: skipEquality
	var valueChanges: ((_ value: String) -> Void)?
	// sourcery:end

	var isEditing: Bool = false
	
	private(set) var rateModel: CurrencyRateModel
	
	private lazy var _decimalFormatter: NumberFormatter = {
		let formatter = NumberFormatter()
		formatter.numberStyle = NumberFormatter.Style.decimal
		formatter.roundingMode = NumberFormatter.RoundingMode.halfUp
		formatter.maximumFractionDigits = 2
		return formatter
	}()
	
	init(rateModel: CurrencyRateModel, defaultValue: Double) {
		self.rateModel = rateModel
		self.value = defaultValue
	}
	
	func updateModelIfNeeded(from rateModel: CurrencyRateModel) {
		if !self.isEditing && self.rateModel.rate != rateModel.rate {
			let newValue = value / self.rateModel.rate * rateModel.rate
			self.rateModel = rateModel
			self.value = newValue
		}
	}
	
	func convert(from baseAmmout: Double) -> Double {
		return rateModel.rate * baseAmmout
	}

	func configure(_ cell: CurrencyCell) {
		cell.titleLabel.text = self.title
		cell.descriptionLabel.text = self.description
		cell.iconImageView.image = self.icon
		cell.valueTextField.text = self.valueString

		cell.cellViewModel = self
	}
}
