//
//  CurrencyCell.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 11/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import UIKit

/// Cell for currency values in table
class CurrencyCell: UITableViewCell {
	
	@IBOutlet weak var iconImageView: UIImageView!

	@IBOutlet weak var titleLabel: UILabel!
	
	@IBOutlet weak var descriptionLabel: UILabel!
	
	@IBOutlet weak var valueTextField: UITextField! {
		didSet {
			valueTextField.delegate = self
			valueTextField.addTarget(self, action: CurrencyCell.editSelector,
									 for: .editingChanged)

		}
	}
	
	var textChangedCallback: ((_ updatedText: String) -> Void)?

	var textDidBeginEditing: (() -> Void)?
	
	var cellViewModel: CurrencyCellViewModel? {
		didSet {
			cellViewModel?.valueChanges = { [weak self] (changedValue: String) in
				DispatchQueue.main.async {
					self?.valueTextField.text = changedValue
				}
			}
		}
	}
}

// MARK: - UITextFieldDelegate
extension CurrencyCell: UITextFieldDelegate {
	
	func textFieldDidBeginEditing(_ textField: UITextField) {
		cellViewModel?.isEditing = true
		textDidBeginEditing?()
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		cellViewModel?.isEditing = false
	}
	
	@objc func textFieldValueChanged(_ textField: UITextField) {
		if let text = textField.text {
			textChangedCallback?(text)
		}
	}
}

fileprivate extension CurrencyCell {
	fileprivate static let editSelector = #selector(CurrencyCell.textFieldValueChanged)
}
