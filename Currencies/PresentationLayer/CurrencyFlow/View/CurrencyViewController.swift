//
//  CurrencyViewController.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 11/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import UIKit
import JGProgressHUD

/// View for currency screen
class CurrencyViewController: UIViewController {

	@IBOutlet weak var currencyListTableView: UITableView! {
		didSet {
			currencyListViewModel?.setTableDelegate(for: currencyListTableView)
			currencyListTableView.tableFooterView = UIView(frame: CGRect.zero)
		}
	}

	@IBOutlet weak var messageView: UIView!
	@IBOutlet weak var messageLabel: UILabel!

	@IBOutlet weak var messageViewHeight: NSLayoutConstraint!

	var currencyListViewModel: CurrencyListViewModel?

	private let _hud = JGProgressHUD(style: .dark)

	private let _messageViewHeightHidden: CGFloat = 0
	private let _messageViewHeightAppear: CGFloat = 60
	private let _animationDuration: Double = 0.5

	private let _loadingHudText = "Loading"

	override func viewDidLoad() {
		super.viewDidLoad()
		currencyListViewModel?.sync()
		observeListModel()

		_hud.textLabel.text = _loadingHudText
		_hud.show(in: self.view, animated: true)
	}

	private func observeListModel() {
		currencyListViewModel?.updateTableViewCallback = { [weak self] in
			guard let `self` = self else {
				return
			}
			DispatchQueue.main.async {
				self._reloadTableView()
			}
		}
		// swiftlint:disable identifier_name
		currencyListViewModel?.moveRowCallback = { [weak self] (at: IndexPath, to: IndexPath) in
			guard let `self` = self else {
				return
			}
			DispatchQueue.main.async {
				self.currencyListTableView.beginUpdates()
				self.currencyListTableView.moveRow(at: at, to: to)
				self.currencyListTableView.endUpdates()
			}
		}
		// swiftlint:enable identifier_name

		currencyListViewModel?.showMessageViewCallback = { [weak self] show, text in
			DispatchQueue.main.async {
				guard let `self` = self else {
					return
				}
				self._showMessageView(show, with: text)
			}
		}
	}

	private func _reloadTableView() {
		if self._hud.isVisible {
			self._hud.dismiss(animated: true)
		}
		self.currencyListTableView.beginUpdates()
		self.currencyListTableView.reloadSections(IndexSet.first, with: .automatic)
		self.currencyListTableView.endUpdates()
	}

	private func _showMessageView(_ show: Bool, with errorText: String?) {
		if let errorText = errorText {
			messageLabel.text = errorText
		}
		let height = show ? _messageViewHeightAppear : _messageViewHeightHidden
		guard messageViewHeight.constant != height && !_hud.isVisible else {
			return
		}
		self.messageViewHeight.constant = height
		self.view.setNeedsLayout()
		UIView.animate(withDuration: _animationDuration, animations: {
			self.view.layoutIfNeeded()
		})
	}
}
