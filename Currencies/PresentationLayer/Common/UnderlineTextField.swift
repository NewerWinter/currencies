//
//  UnderlineTextField.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 13/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import UIKit

final class UnderlineTextField: UITextField {

	private var lineView: UIView

	private lazy var lineViewHeight: CGFloat = {
		let onePixel: CGFloat = 1.0 / UIScreen.main.scale
		let lineHeight = 3.0 * onePixel
		return lineHeight
	}()

	override public init(frame: CGRect) {
		lineView = UnderlineTextField.createLineView()

		super.init(frame: frame)

		addSubview(lineView)
	}

	required init?(coder aDecoder: NSCoder) {
		lineView = UnderlineTextField.createLineView()

		super.init(coder: aDecoder)

		addSubview(lineView)
	}

	override func layoutSubviews() {
		super.layoutSubviews()
		
		lineView.frame = calculateLineViewFrame(lineView, lineHeight: lineViewHeight)
	}

	private func calculateLineViewFrame(_ lineView: UIView, lineHeight: CGFloat) -> CGRect {
		let newFrame = CGRect(x: 0,
							  y: bounds.size.height - lineHeight,
							  width: bounds.size.width,
							  height: lineHeight)
		return newFrame
	}

	private static func createLineView() -> UIView {
		let lineView = UIView()
		lineView.isUserInteractionEnabled = false
		lineView.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
		lineView.backgroundColor = UIColor.lightGray
		return lineView
	}

	private static func configureDefaultLineHeight() -> CGFloat {
		let onePixel: CGFloat = 1.0 / UIScreen.main.scale
		let lineHeight = 2.0 * onePixel
		return lineHeight
	}
	
}
