//
//  CurrencyService.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 11/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import Foundation

/// Input interface for currrency service
protocol CurrencyServiceInput: AutoMockable {
	
	typealias SuccessCallback = (_ currencyModel: CurrencyModel) -> Void
	typealias FailureCallback = (_ errorText: String) -> Void

	var isLoading: Bool { get }
	
	func get(
		for currency: Currency,
		successCallback: @escaping SuccessCallback,
		failureCallback: @escaping FailureCallback)
}

final class CurrencyService: CurrencyServiceInput {

	private(set) var isLoading: Bool = false

	private var _networkManager: Network.Manager<CurrencyModel>

	init(networkManager: Network.Manager<CurrencyModel>) {
		self._networkManager = networkManager
	}
	
	func get(
		for currency: Currency,
		successCallback: @escaping CurrencyServiceInput.SuccessCallback,
		failureCallback: @escaping CurrencyServiceInput.FailureCallback) {
		let apiCurrency = API.Currency.get(currency: currency.rawValue)
		
		let successNetworkCallback = { [weak self] (currencyModel: CurrencyModel) in
			self?.isLoading = false
			successCallback(currencyModel)
		}
		
		let failureNetworkCallback = { [weak self] (error: Network.Error) in
			self?.isLoading = false
			failureCallback(error.localizedDescription)
		}
		isLoading = true
		_networkManager.requestObject(with: apiCurrency,
													 successCallback: successNetworkCallback,
													 failureCallback:failureNetworkCallback)
	}
}
