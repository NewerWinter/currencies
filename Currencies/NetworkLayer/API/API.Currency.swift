//
//  API.Currency.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 11/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import Foundation
import Alamofire

extension API {
	/// Currency variaty of routes
	enum Currency {
		static let url = API.url
		case get(currency: String)
	}
}

// MARK: - APIMethodProtocol
extension API.Currency: APIMethodProtocol {
	
	var method: Alamofire.HTTPMethod {
		return .get
	}
	
	var path: String {
		return API.Currency.url + "/latest"
	}
	
	var params: [String: Any]? {
		switch self {
		case .get(let currency):
			return [_baseKey: currency]
		}
	}

	private var _baseKey: String {
		return "base"
	}
}
