//
//  API.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 11/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import Foundation
import Alamofire

/// Enum for type safe request ceation
public enum API {
	static let url = "https://revolut.duckdns.org"
}

/// Protocol that must conform each route to be transformed to URLRequest
public protocol APIMethodProtocol: URLRequestConvertible {
	var method: Alamofire.HTTPMethod { get }
	var path: String { get }
	var params: [String : Any]? { get }
}

/// Default implemetation
public extension APIMethodProtocol {

	/// Create urlREquest from enum
	///
	/// - Returns: ready for use url request
	/// - Throws: Create error
	public func asURLRequest() throws -> URLRequest {
		guard let url =  URL(string: path) else {	
			throw Network.Error.invalidURL
		}
		var request = URLRequest(url: url)
		request.httpMethod = method.rawValue
		
		switch method {
		case .post, .put:
			return try Alamofire.JSONEncoding.default.encode(request, with: params)
		default:
			return try Alamofire.URLEncoding.default.encode(request, with: params)
		}
	}
}
