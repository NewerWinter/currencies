//
//  Network.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 11/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

/// Namespace for network objects
public struct Network {
	/// Handle network requests
	public class Manager<ResultType: ImmutableMappable> {
		// swiftlint:disable nesting
		/// Network reponse type
		public typealias Response = DataResponse<ResultType>

		/// Success callback with result data
		public typealias Success = (_ response: ResultType) -> Void

		/// Failure callback type with network error
		public typealias Failure = (_ error: Network.Error) -> Void

		// swiftlint:disable enable

		/// Make network request wit success and failure callback
		///
		/// - Parameters:
		///   - request: request parameters
		///   - successCallback: call on success getting data
		///   - failureCallback: call on fail with error
		public func requestObject(
			with request: URLRequestConvertible,
			successCallback: @escaping Success,
			failureCallback: @escaping Failure) {
			let requestDebug = Alamofire.request(request)
				.validate()
				.responseObject { (response: Response) in
					switch response.result {
					case .success(let object):
						successCallback(object)
					case .failure(let error):
						if let networkError = error as? Network.Error {
							return failureCallback(networkError)
						}
						let message = error.localizedDescription
						let error = Network.Error.customError(
							title: "Network error",
							message: message)
						failureCallback(error)
					}
			}
			#if DEBUG
			debugPrint(requestDebug)
			#endif
		}
	}
}
