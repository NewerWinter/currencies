//
//  Network.Error.swift
//  Currencies
//
//  Created by Aleksandr Lavrinenko on 11/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import Foundation

/// Network error declaration
public extension Network {
	
	/// Erros that can happen during Network wrok
	///
	/// - invalidURL: cannot patse URL to start request
	/// - customError: customizable error
	public enum Error: Swift.Error, CustomStringConvertible {
		case invalidURL
		case parseError
		case loadingError
		case customError(title: String, message: String?)
		
		var localizedDescription: String {
			switch self {
			case .invalidURL, .parseError, .loadingError, .customError:
				return _defaultErrorMessage
			}
		}

		/// Description for debugging
		public var description: String {
			switch self {
			case .invalidURL:
				return _invalidURLDescription
			case .parseError:
				return _parseErrorDescription
			case .loadingError:
				return _loadingErrorDescription
			case let .customError(title, message):
				return "\(title):\(message ?? "")"
			}
		}

		private var _invalidURLDescription: String {
			return "Error while creating URL"
		}

		private var _parseErrorDescription: String {
			return "Error while parsing data"
		}

		private var _loadingErrorDescription: String {
			return "Error while trying to reach the server"
		}

		private var _defaultErrorMessage: String {
			return "Could reach our server. Rates can be not actual. This message will dissapear after reconnection."
		}
	}
}
