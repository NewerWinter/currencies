//
//  CurrencyServiceTests.swift
//  CurrenciesTests
//
//  Created by Aleksandr Lavrinenko on 13/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import XCTest
@testable import Currencies
@testable import Alamofire
@testable import ObjectMapper

class CurrencyServiceTests: XCTestCase {

	private class SuccessNetworkManagerMock : Network.Manager<CurrencyModel> {

		var mockCurrecnyModel: CurrencyModel

		init(mockCurrecnyModel: CurrencyModel) {
			self.mockCurrecnyModel = mockCurrecnyModel
		}

		override func requestObject(
			with request: URLRequestConvertible,
			successCallback: @escaping (CurrencyModel) -> Void,
			failureCallback: @escaping Network.Manager<CurrencyModel>.Failure) {
			let mockCurrencyModel = CurrencyModel.mock()
			successCallback(mockCurrencyModel)
		}
	}

	private final class FailureParseNetworkManagerMock : Network.Manager<CurrencyModel> {
		override func requestObject(
			with request: URLRequestConvertible,
			successCallback: @escaping (CurrencyModel) -> Void,
			failureCallback: @escaping Network.Manager<CurrencyModel>.Failure) {
			failureCallback(Network.Error.parseError)
		}
	}

	func testSuccessCallback() {
		// arrange
		let mockCurrencyModel = CurrencyModel.mock()
		let successNetworkManagerMock = SuccessNetworkManagerMock(mockCurrecnyModel: mockCurrencyModel)
		let service: CurrencyServiceInput = CurrencyService(networkManager: successNetworkManagerMock)
		let currency = Currency.EUR

		// Expectation for the closure we're expecting to be completed
		let completedExpectation = expectation(description: "Failure")

		let successCallback = { (currencyModel: CurrencyModel) in
			completedExpectation.fulfill()
			XCTAssertEqual(mockCurrencyModel, currencyModel)
		}

		let failureCallback = { (errorText: String) in
			XCTAssert(true, "Should call successCallback, not failure")
		}

		// act
		service.get(for: currency,
					successCallback: successCallback,
					failureCallback: failureCallback)

		// assert
		waitForExpectations(timeout: 0.3, handler: nil)
	}

	func testFailureCallback() {
		// arrange
		let failureNetworkManagerMock = FailureParseNetworkManagerMock()
		let service: CurrencyServiceInput = CurrencyService(networkManager: failureNetworkManagerMock)
		let currency = Currency.EUR

		// Expectation for the closure we're expecting to be completed
		let completedExpectation = expectation(description: "Failure")

		let successCallback = { (currencyModel: CurrencyModel) in
			XCTAssert(true, "Should call failureCallback, not success")
		}

		let failureCallback = { (errorText: String) in
			completedExpectation.fulfill()
			XCTAssertEqual(errorText, Network.Error.loadingError.localizedDescription)
		}
		// act
		service.get(for: currency,
					successCallback: successCallback,
					failureCallback: failureCallback)

		// assert
		waitForExpectations(timeout: 0.3, handler: nil)
	}

}
