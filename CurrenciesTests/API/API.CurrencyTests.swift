//
//  API.swift
//  CurrenciesTests
//
//  Created by Aleksandr Lavrinenko on 13/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import XCTest
@testable import Currencies
@testable import Alamofire

class APICurrencyTests: XCTestCase {

	private let _currencyString = Currency.EUR.rawValue

	private var _apiCurrency: API.Currency!

	override func setUp() {
		super.setUp()
		_apiCurrency = API.Currency.get(currency: _currencyString)
	}

    func testInit() {
		// assert
		XCTAssertEqual(_apiCurrency.method, Alamofire.HTTPMethod.get)
		XCTAssertEqual(_apiCurrency.path, API.Currency.url + "/latest")
		XCTAssertNotNil(_apiCurrency.params)
		XCTAssertEqual(_apiCurrency.params?["base"] as? String, _currencyString)
		XCTAssert(_apiCurrency.params?.count == 1)
    }

	func testURLRequestCreationSuccess() {
		do {
			let urlRequest = try _apiCurrency.asURLRequest()
			XCTAssertNotNil(urlRequest)
			XCTAssertEqual(urlRequest.httpMethod, "GET")
		} catch {
			XCTAssertTrue(true, "Should be success")
		}
	}
}
