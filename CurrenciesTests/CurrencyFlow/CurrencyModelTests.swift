//
//  CurrencyModelTests.swift
//  CurrenciesTests
//
//  Created by Aleksandr Lavrinenko on 14/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import Currencies

class CurrencyModelTests: XCTestCase {

    func testInitSuccess() {
		// arrange
		let map = Map(mappingType: .fromJSON, JSON: _successJSON)
		let rateModelEUR = CurrencyRateModel(currency: _baseCurrency,
											 rate: _baseRateValue)
		let rateModelAUD = CurrencyRateModel(currency: _rateCurrency,
										  rate: _rateValue)
		let rateModelList = [rateModelEUR, rateModelAUD]

		// act
		do {
			let currencyModel = try CurrencyModel(map: map)
			// assert
			XCTAssertEqual(currencyModel.base, Currency.EUR)
			XCTAssertEqual(currencyModel.date, _dateValue)
			XCTAssertEqual(currencyModel.rateList, rateModelList)
		} catch {
			// assert
			XCTAssertTrue(true, "should be successed")
		}
    }

    func testInitFailParseBase() {
		// arrange
		let map = Map(mappingType: .fromJSON, JSON: _failBaseJSON)

		// act
		do {
			let currencyModel = try CurrencyModel(map: map)
			// assert
			XCTAssertNil(currencyModel, "should not call")
		} catch Network.Error.parseError {

		} catch {
			// assert
			XCTAssertTrue(true, "should be failed")
		}
    }

	func testInitFailParseDate() {
		// arrange
		let map = Map(mappingType: .fromJSON, JSON: _failDateJSON)

		// act
		do {
			let currencyModel = try CurrencyModel(map: map)
			// assert
			XCTAssertNil(currencyModel, "should not call")
		} catch Network.Error.parseError {

		} catch {
			// assert
			XCTAssertTrue(true, "should be failed")
		}
	}

	func testInitFailParseRate() {
		// arrange
		let map = Map(mappingType: .fromJSON, JSON: _successJSON)

		// act
		do {
			let currencyModel = try CurrencyModel(map: map)
			// assert
			XCTAssertFalse(currencyModel.rateList.count == 1)
		} catch {
			// assert
			XCTAssertTrue(true, "should be failed")
		}
	}

	private var _successJSON: [String: Any] {
		return ["base": _baseCurrency.rawValue,
				"date": _dateValue,
				"rates": [_rateCurrency.rawValue: _rateValue]]
	}

	// MARK: - success data

	private var _dateValue: String {
		return "10-10-2020"
	}

	private var _baseCurrency: Currency {
		return Currency.EUR
	}

	private var _rateCurrency: Currency {
		return Currency.AUD
	}

	private var _rateValue: Double {
		return 1.1
	}

	private var _baseRateValue: Double {
		return 1.0
	}

	// MARK: - failure data

	private var _failBaseJSON: [String: Any] {
		return ["base": _baseWrongCurrencyString,
				"date": _dateValue,
				"rates": [_rateCurrency.rawValue: _rateValue]]
	}

	private var _failDateJSON: [String: Any] {
		return ["base": _baseWrongCurrencyString,
				"date": _wrongDateCurrencyString,
				"rates": [_rateCurrency.rawValue: _rateValue]]
	}

	private var _failRatesJSON: [String: Any] {
		return ["base": _baseWrongCurrencyString,
				"date": _dateValue,
				"rates": [_wrongRateCurrencyString: _rateValue]]
	}

	private var _baseWrongCurrencyString: String {
		return "Fake currency"
	}

	private var _wrongDateCurrencyString: Int {
		return 100
	}

	private var _wrongRateCurrencyString: String {
		return "Fake currency"
	}

}
