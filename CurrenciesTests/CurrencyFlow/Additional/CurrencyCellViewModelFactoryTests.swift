//
//  CurrencyCellViewModelFactoryTests.swift
//  CurrenciesTests
//
//  Created by Aleksandr Lavrinenko on 14/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import XCTest
@testable import Currencies

class CurrencyCellVMFactoryTests: XCTestCase {

    func testCreateCellViewModelList() {
		// arrange
		let cellViewModelFactory = CurrencyCellViewModelFactory()
		let mockCurrencyModel = CurrencyModel.mock()

		// act
		let cellViewModelList = cellViewModelFactory.createCellViewModelList(from: mockCurrencyModel)

		//assert
		XCTAssertEqual(cellViewModelList.count, mockCurrencyModel.rateList.count)
		for (index, rate) in mockCurrencyModel.rateList.enumerated() {
			let cellViewModel = cellViewModelList[index]
			XCTAssertEqual(cellViewModel.currency, rate.currency)
			XCTAssertEqual(cellViewModel.rateModel.rate, rate.rate)
		}
    }

    func testCreateCellViewModel() {
		// arrange
		let cellViewModelFactory = CurrencyCellViewModelFactory()
		let mockCurrencyRateModel = CurrencyRateModel.mock()

		// act
		let cellViewModel = cellViewModelFactory.createCellViewModel(from: mockCurrencyRateModel)

		// assert
		XCTAssertEqual(cellViewModel.currency, mockCurrencyRateModel.currency)
		XCTAssertEqual(cellViewModel.rateModel.rate, mockCurrencyRateModel.rate)
    }

}
