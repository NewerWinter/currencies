//
//  CurrencyListViewModelTests.swift
//  CurrenciesTests
//
//  Created by Aleksandr Lavrinenko on 14/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import XCTest
@testable import Currencies

class CurrencyListViewModelTests: XCTestCase {

	private var listViewModel: CurrencyListViewModel!
	private var mockCurrencyService: CurrencyServiceInputMock!
	private var mockCurrencyFactory: CurrencyCellViewModelFactoryInputMock!
	private var mockCurrencyTableManager: CurrencyTableViewManager!

	private let _deafultAmoutValue = 100.0

	override func setUp() {
		super.setUp()
		mockCurrencyService = CurrencyServiceInputMock()
		mockCurrencyFactory = CurrencyCellViewModelFactoryInputMock()
		listViewModel = CurrencyListViewModel(currencyServcie: mockCurrencyService,
											  cellFactory: mockCurrencyFactory)
		mockCurrencyTableManager = CurrencyTableViewManager(delegate: listViewModel)
		listViewModel.setTableViewManager(mockCurrencyTableManager)
	}

	override func tearDown() {
		mockCurrencyService = nil
		mockCurrencyFactory = nil
		listViewModel = nil
		mockCurrencyTableManager = nil
		super.tearDown()
	}

	func testStartSync() {
		// arrange
		let currencyModel = CurrencyModel.mock()
		let cellViewModelList = _cellViewModeList(from: currencyModel)

		let expectedNumberOfRows = cellViewModelList.count
		let section = 0

		// act (internally calls startObserving function)
		_loadViewModelList(with: currencyModel, return: cellViewModelList)

		// assert
		XCTAssertTrue(mockCurrencyService
			.getForSuccessCallbackFailureCallbackCalled)
		XCTAssertTrue(mockCurrencyFactory.createCellViewModelListFromCalled)
		XCTAssertEqual(listViewModel.numberOfRows(forSection: section), expectedNumberOfRows)

	}

	func testStartSyncLoaddataFailed() {
		// arrange
		let errorString = "Network Error"
		mockCurrencyService.underlyingIsLoading = false

		// act
		listViewModel.sync()

		let serviceCallbackExpect = expectation(description: "Service success callback not called")
		mockCurrencyService.getForSuccessCallbackFailureCallbackClosure = { (_, _, _) in
			serviceCallbackExpect.fulfill()
		}
		listViewModel.showMessageViewCallback = { show, text in
			// assert
			XCTAssertEqual(text, errorString)
			XCTAssertTrue(show)
		}
		// Wait for service success callback complete. We need min 1 second for timer trigger
		wait(for: [serviceCallbackExpect], timeout: 1.5)
		mockCurrencyService.getForSuccessCallbackFailureCallbackClosure = nil
		mockCurrencyService
			.getForSuccessCallbackFailureCallbackReceivedArguments?.failureCallback(errorString)

		// assert
		XCTAssertTrue(mockCurrencyService
			.getForSuccessCallbackFailureCallbackCalled)
		XCTAssertFalse(mockCurrencyFactory.createCellViewModelListFromCalled)
	}

	func testNumberOfRows() {
		// arrange
		let currencyModel = CurrencyModel.mock()
		let cellViewModelList = _cellViewModeList(from: currencyModel)

		let expectedNumberOfRows = cellViewModelList.count
		let section = 0

		// act (internally calls sync function)
		_loadViewModelList(with: currencyModel, return: cellViewModelList)

		// assert
		XCTAssertEqual(listViewModel.numberOfRows(forSection: section), expectedNumberOfRows)
	}

	func testCurrencyViewModelForRow() {
		// arrange
		let currencyModel = CurrencyModel.mock()
		let cellViewModelList = _cellViewModeList(from: currencyModel)
		let secondIndexPath = IndexPath(row: 1, section: 0)

		// act (internally calls sync function)
		_loadViewModelList(with: currencyModel, return: cellViewModelList)

		// assert
		XCTAssertEqual(listViewModel.currencyViewModel(forRowAt: IndexPath.first), cellViewModelList.first)
		XCTAssertEqual(listViewModel.currencyViewModel(forRowAt: secondIndexPath), cellViewModelList.last)
	}

	func testSetTableViewDelegate() {
		// arrange
		let tableView = UITableView(frame: CGRect.zero)

		// act
		listViewModel.setTableDelegate(for: tableView)

		// assert
		XCTAssertTrue(tableView.delegate is CurrencyTableViewManager)
		XCTAssertTrue(tableView.dataSource is CurrencyTableViewManager)
	}

	func testCellSelected() {
		// arrange
		let updateCallbackExpect = expectation(description: "Move raw callback not called")
		let selectedIndexPath = IndexPath(row: 1, section: 0)
		listViewModel.moveRowCallback = { (fromPath: IndexPath, toPath: IndexPath) in
			updateCallbackExpect.fulfill()
			// assert
			XCTAssertEqual(toPath, IndexPath.first)
			XCTAssertEqual(fromPath, selectedIndexPath)
		}

		// act
		listViewModel.currencyCellSelected(at: selectedIndexPath)
		
		// assert
		wait(for: [updateCallbackExpect], timeout: 1)
	}

	func testCurrencyViewModelEdited() {
		// arrange
		let currencyModel = CurrencyModel.mock()
		let cellViewModelList = _cellViewModeList(from: currencyModel)
		_loadViewModelList(with: currencyModel, return: cellViewModelList)
		let newAmountValue = "120"
		let expectedValue = "109.09"
		cellViewModelList.last?.isEditing = true

		// act
		listViewModel.currencyViewModelEditing(cellViewModelList.last!,
											  newAmountValue: newAmountValue)

		// assert
		let editedViewModelFirst = listViewModel.currencyViewModel(forRowAt: IndexPath.first)
		let editedViewModelSecond = listViewModel.currencyViewModel(forRowAt: IndexPath(row: 1, section: 0))
		XCTAssertEqual(editedViewModelFirst.valueString, expectedValue)
		XCTAssertEqual(editedViewModelSecond.valueString, newAmountValue)
	}

	private func _loadViewModelList(with currencyModel: CurrencyModel, return viewModelList: [CurrencyCellViewModel]) {
		mockCurrencyService.underlyingIsLoading = false
		mockCurrencyFactory.createCellViewModelListFromReturnValue = viewModelList

		listViewModel.sync()

		let serviceCallbackExpect = expectation(description: "Service success callback not called")
		mockCurrencyService.getForSuccessCallbackFailureCallbackClosure = { (_, _, _) in
			serviceCallbackExpect.fulfill()
		}
		// Wait for service success callback complete. We need min 1 second for timer trigger
		wait(for: [serviceCallbackExpect], timeout: 1.5)
		mockCurrencyService.getForSuccessCallbackFailureCallbackClosure = nil
		mockCurrencyService
			.getForSuccessCallbackFailureCallbackReceivedArguments?.successCallback(currencyModel)
	}

	private func _cellViewModeList(from currencyModel: CurrencyModel) -> [CurrencyCellViewModel] {
		let viewModelList = currencyModel.rateList.map { (rateModel) -> CurrencyCellViewModel in
			return CurrencyCellViewModel(rateModel: rateModel, defaultValue: _deafultAmoutValue)
		}
		return viewModelList
	}
}
