//
//  CurrencyCellViewModelTests.swift
//  CurrenciesTests
//
//  Created by Aleksandr Lavrinenko on 14/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

import XCTest
@testable import Currencies

class CurrencyCellViewModelTests: XCTestCase {

	var mockRateModel: CurrencyRateModel!
	var cellViewModel: CurrencyCellViewModel!

	let defaultValue: Double = 100.0

	override func setUp() {
		super.setUp()
		mockRateModel = CurrencyRateModel.mock()
		cellViewModel = CurrencyCellViewModel(rateModel: mockRateModel, defaultValue: defaultValue)
	}

	override func tearDown() {
		mockRateModel = nil
		cellViewModel = nil
		super.tearDown()
	}

	func testInit() {
		// arrange
		let defaultValueString: String = "100"

		// act
		let cellViewModel = CurrencyCellViewModel(rateModel: mockRateModel, defaultValue: defaultValue)

		// assert
		XCTAssertEqual(cellViewModel.currency, mockRateModel.currency)
		XCTAssertEqual(cellViewModel.title, mockRateModel.currency.rawValue)
		XCTAssertEqual(cellViewModel.description,
					   mockRateModel.currency.description)
		XCTAssertEqual(cellViewModel.description,
					   mockRateModel.currency.description)
		XCTAssertEqual(cellViewModel.icon,
					   mockRateModel.currency.icon)
		XCTAssertEqual(cellViewModel.valueString,
					   defaultValueString)
		XCTAssertFalse(cellViewModel.isEditing)
	}

	func testUpdateModelNeeded() {
		// arrange
		let currency = Currency.EUR
		let newRate = 1.3
		let newRateModel = CurrencyRateModel(currency: currency, rate: newRate)
		let newValueString = "108.33"

		// act
		cellViewModel.updateModelIfNeeded(from: newRateModel)

		// assert
		XCTAssertEqual(cellViewModel.rateModel, newRateModel)
		XCTAssertEqual(cellViewModel.valueString, newValueString)
	}

	func testConvert() {
		// arrange
		let baseAmount = 100.0
		let newAmount = mockRateModel.rate * baseAmount
		// act
		let convertResult = cellViewModel.convert(from: baseAmount)

		// assert
		XCTAssertEqual(convertResult, newAmount)
	}

	func testConfigureCell() {
		// arrange
		let currencyCell = CurrencyCell(frame: CGRect.zero)

		let iconImageView = UIImageView(frame: CGRect.zero)
		let titleLabel = UILabel(frame: CGRect.zero)
		let descriptionLabel = UILabel(frame: CGRect.zero)
		let valueTextField = UITextField(frame: CGRect.zero)

		currencyCell.iconImageView = iconImageView
		currencyCell.titleLabel = titleLabel
		currencyCell.descriptionLabel = descriptionLabel
		currencyCell.valueTextField = valueTextField

		// act
		cellViewModel.configure(currencyCell)

		// assert
		XCTAssertEqual(currencyCell.iconImageView.image, cellViewModel.icon)
		XCTAssertEqual(currencyCell.titleLabel.text, cellViewModel.title)
		XCTAssertEqual(currencyCell.descriptionLabel.text, cellViewModel.description)
		XCTAssertEqual(currencyCell.valueTextField.text, cellViewModel.valueString)
		XCTAssertEqual(currencyCell.cellViewModel, cellViewModel)
	}
}
