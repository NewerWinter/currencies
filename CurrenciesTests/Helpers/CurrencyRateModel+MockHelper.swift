//
//  CurrencyRateModel.swift
//  CurrenciesTests
//
//  Created by Aleksandr Lavrinenko on 14/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

@testable import Currencies
import ObjectMapper

extension CurrencyRateModel {
	
	static func mock() -> CurrencyRateModel {
		let currency = Currency.EUR
		let rate = 1.2
		let mockRate = CurrencyRateModel(currency: currency, rate: rate)
		return mockRate
	}
}
