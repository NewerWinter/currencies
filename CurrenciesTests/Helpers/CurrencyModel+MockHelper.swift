//
//  CurrencyModel+MockHelper.swift
//  CurrenciesTests
//
//  Created by Aleksandr Lavrinenko on 14/02/2019.
//  Copyright © 2019 Aleksandr Lavrinenko. All rights reserved.
//

@testable import Currencies
import ObjectMapper

extension CurrencyModel {
	
	static func mock() -> CurrencyModel {
		let map = CurrencyModel.mockMap()
		guard let mockModel = try? CurrencyModel(map: map) else {
			fatalError("Cannot crate CurrencyModel mock")
		}
		return mockModel
	}

	private static func mockMap() -> Map {
		let mockJSON: [String: Any] = ["base": "EUR", "date": "10-10-2020", "rates": ["AUD": 1.1]]
		let map = Map(mappingType: .fromJSON, JSON: mockJSON)
		return map
	}
}
